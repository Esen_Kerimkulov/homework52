import React, { Component } from 'react';
import './App.css';

class App extends Component {
  state = {
    numbers: []
  };

  getRandomNumbers = () => {
    let arr = [];

    while (arr.length < 5){
      let number = Math.floor(Math.random() * (36 - 5) + 5);
      if (!arr.includes(number)) arr.push(number);
    }

    arr.sort(function (a, b) {
      return a - b;
    });

    this.setState({numbers: arr})
};

  clickUpdate = () => {
    this.getRandomNumbers()
  };


  render() {
    return (
        <div>
          <button className="btn" onClick={this.clickUpdate}>New numbers</button>
          {this.state.numbers.map((number, key )=> {
            return <p className="numb" key={key}>{number}</p>
          })}
        </div>
    )
  }

}



export default App;
